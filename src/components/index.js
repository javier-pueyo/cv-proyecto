import Hero from './hero/hero';
import AboutUs from './about-us/about-us';
import Contact from './contact/contact';
import Knowledge from './knowledge/knowledge';
import Timeline from './timeline/timeline';
import Projects from './projects/projects';

export { Hero, AboutUs, Knowledge, Timeline, Contact, Projects };