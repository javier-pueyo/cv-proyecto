import Footer from './footer/footer';
import Header from './header/header';
import Nav from './nav/nav';

export {Header, Footer, Nav};